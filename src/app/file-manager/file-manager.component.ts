import { Component, Input, OnInit, AfterViewInit, SimpleChanges, OnChanges, Output, EventEmitter } from '@angular/core';
import { FileSystemObject, UploadFileInformation, StorageSpace, Breadcrumb, ShareOption } from './file-manager.model';
import { FormGroup, FormBuilder, Validators } from '../../../node_modules/@angular/forms';
import { DomSanitizer } from '../../../node_modules/@angular/platform-browser';

declare let M: any;

@Component({
    selector: 'file-manager',
    templateUrl: './file-manager.component.html',
    styleUrls: ['./file-manager.component.css']
})

export class FileManagerComponent implements OnInit, OnChanges, AfterViewInit {

    private timeoutHandler: number;
    private mobileBrowserIdentifier: any;
    private cutFileSystemObject: FileSystemObject;
    private cutBreadcrumbs: Breadcrumb[];
    private uploadFileInformation: UploadFileInformation;
    private documentBlob: Blob;

    newDirectoryForm: FormGroup;
    renameForm: FormGroup;

    isMobileBrowser: boolean;
    isTouching: boolean;

    @Input() fileSystemObjects: FileSystemObject[];
    @Input() storageSpace: StorageSpace;
    @Input() breadcrumbs: Breadcrumb[];
    @Input() shareOptions: ShareOption[];
    @Output('changeDirectory') changeDirectory = new EventEmitter<string>();
    @Output('newDirectory') newDirectory = new EventEmitter<any>();
    @Output('uploadDocument') uploadDocument = new EventEmitter<any>();
    @Output('deleteFileSystemObject') deleteFileSystemObject = new EventEmitter<any>();
    @Output('renameFileSystemObject') renameFileSystemObject = new EventEmitter<any>();
    @Output('downloadFile') downloadFile = new EventEmitter<any>();
    @Output('pasteFileSystemObject') pasteFileSystemObject = new EventEmitter<any>();

    // constructor(private _script: ScriptLoaderService, private _fb: FormBuilder) {
    constructor(private _fb: FormBuilder, private _sanitizer: DomSanitizer) {
        this.timeoutHandler = 600;
        this.isTouching = false;
        this.PopulateMobileBrowserIdentifier();
        this.CheckIfMobileOrTabletBrowser();
        this.NewDirectoryFormInit();
        this.RenameFormInit();
        this.shareOptions = [];
        this.fileSystemObjects = [];
        this.breadcrumbs = [];
        this.cutBreadcrumbs = [];
        this.storageSpace = new StorageSpace({});
        this.uploadFileInformation = new UploadFileInformation({});
        this.PopulateShareOptions();
        this.PopulateFileSystemObject();
        this.PopulateStorageSpace();
        this.PopulateBreadcrumbs();
    }

    private NewDirectoryFormInit() {
        this.newDirectoryForm = this._fb.group({
            name: ['', [Validators.required]],
        });
    }

    private RenameFormInit() {
        this.renameForm = this._fb.group({
            name: ['', [Validators.required]],
        });
    }

    private PopulateMobileBrowserIdentifier() {
        this.mobileBrowserIdentifier = {
            regex1: /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i,
            regex2: /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i
        };
    }

    private CheckIfMobileOrTabletBrowser() {
        if (this.mobileBrowserIdentifier.regex1.test(navigator.userAgent) ||
            this.mobileBrowserIdentifier.regex1.test(navigator.vendor) ||
            this.mobileBrowserIdentifier.regex2.test(navigator.userAgent.slice(0, 4)) ||
            this.mobileBrowserIdentifier.regex2.test(navigator.vendor.slice(0, 4))) {
            this.isMobileBrowser = true;
        } else {
            this.isMobileBrowser = false;
        }
    }

    private ToogleFileSystemObjectSelectStatus(fileSystemObject: FileSystemObject) {
        if (fileSystemObject.isSelected) {
            fileSystemObject.isSelected = false;
        } else {
            this.fileSystemObjects.filter(x => x.isSelected === true).map(x => x.isSelected = false);
            fileSystemObject.isSelected = true;
        }
    }

    private ViewDocument(fileSystemObject: FileSystemObject) {
        var docExtensions = ['ppt', 'pptx', 'doc', 'docx', 'xls', 'xlsx', 'pdf'];
        if (docExtensions.indexOf(fileSystemObject.extension) !== -1) {
            window.open('https://drive.google.com/viewer?embedded=true&url=' + fileSystemObject.fileUrl);
        } else {
            +
                window.open(fileSystemObject.fileUrl);
        }
    }

    private PopulateShareOptions() {
        this.shareOptions = [];
        this.shareOptions.push(new ShareOption({
            id: '1',
            name: 'Private',
            description: 'Only uploader can see.',
            hasDropDown: false,
            dropDownValue: '',
            dropDownOption: []
        }));
        this.shareOptions.push(new ShareOption({
            id: '2',
            name: 'Specific Users',
            description: 'Only uploader and sharing users can see.',
            hasDropDown: true,
            dropDownValue: '',
            dropDownOption: []
        }));
        this.shareOptions.push(new ShareOption({
            id: '3',
            name: 'Team',
            description: 'Everyone in the legacy team can see.',
            hasDropDown: false,
            dropDownValue: '',
            dropDownOption: []
        }));
    }

    private PopulateStorageSpace() {
        this.storageSpace = new StorageSpace({
            limit: 1095.68,
            limitUnit: 'KB',
            displayLimit: 1.07,
            displayLimitUnit: 'MB',
            used: 190.51,
            usedUnit: 'KB',
            displayUsed: 190.51,
            displayUsedUnit: 'KB'
        });
    }

    private PopulateFileSystemObject() {
        this.fileSystemObjects = [];
        this.fileSystemObjects.push(new FileSystemObject({
            name: 'Assets',
            extension: '',
            displayName: 'Assets',
            locked: true,
            isShared: false,
            isVisiable: true,
            type: 'directory'
        }));
        this.fileSystemObjects.push(new FileSystemObject({
            name: 'Insurance Documents',
            extension: '',
            displayName: 'Insurance Documents',
            locked: true,
            isShared: false,
            isVisiable: true,
            type: 'directory'
        }));
        this.fileSystemObjects.push(new FileSystemObject({
            name: 'Medical Directives',
            extension: '',
            displayName: 'Medical Directives',
            locked: true,
            isShared: false,
            isVisiable: true,
            type: 'directory'
        }));
        this.fileSystemObjects.push(new FileSystemObject({
            name: 'Trust Documents',
            extension: '',
            displayName: 'Trust Documents',
            locked: true,
            isShared: false,
            isVisiable: true,
            type: 'directory'
        }));
        this.fileSystemObjects.push(new FileSystemObject({
            name: 'Test Dir',
            extension: '',
            displayName: 'Test Dir',
            locked: false,
            isShared: true,
            isVisiable: true,
            type: 'directory'
        }));
        this.fileSystemObjects.push(new FileSystemObject({
            name: 'tablerates',
            extension: 'csv',
            displayName: 'tablerates.csv',
            locked: false,
            isShared: false,
            isVisiable: true,
            type: 'file'
        }));
    }

    private PopulateBreadcrumbs() {
        this.breadcrumbs = [];
        this.breadcrumbs.push(new Breadcrumb({
            name: 'root',
            relativeUrl: '/'
        }));
        this.breadcrumbs.push(new Breadcrumb({
            name: 'Test Dir',
            relativeUrl: '/test dir/'
        }));

    }

    ngOnInit() {

    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.breadcrumbs && this.breadcrumbs && this.breadcrumbs.length > 0) {
            //console.log(this.breadcrumbs);
        }
        if (changes.fileSystemObjects && this.fileSystemObjects && this.fileSystemObjects.length > 0) {
            if (this.cutFileSystemObject && this.fileSystemObjects.find(x => x.id === this.cutFileSystemObject.id)) {
                this.fileSystemObjects.find(x => x.id === this.cutFileSystemObject.id).isCut = true;
            }
        }
    }

    ngAfterViewInit() {
        // this._script.loadScripts('body',
        //     ['assets/app/js/materialize.min.js']);
        M.AutoInit();
    }

    getFileSystemObjectClass(fileSystemObject: FileSystemObject) {
        return {
            shared: fileSystemObject.isShared ? fileSystemObject.isShared : false,
            folder: fileSystemObject.type === 'directory',
            file: fileSystemObject.type === 'file',
            locked: fileSystemObject.locked ? fileSystemObject.locked : false,
            cut: fileSystemObject.isCut ? fileSystemObject.isCut : false,
            'ui-selected': fileSystemObject.isSelected ? fileSystemObject.isSelected : false
        };
    }

    getFileSystemObjectIconClass(fileSystemObject: FileSystemObject) {
        return {
            icon: true,
            fas: true,
            'fa-folder-open': fileSystemObject.type === 'directory',
            'fa-file': fileSystemObject.type === 'file'
        };
    }

    getLevelUpButtonClass() {
        return {
            fas: true,
            'fa-level-up-alt': true,
            disabled: !this.breadcrumbs || this.breadcrumbs.length <= 1
        };
    }

    getCutButtonClass() {
        const selectedfileSystemObject = this.fileSystemObjects ? this.fileSystemObjects.find(x => x.isSelected) : null;
        return {
            fas: true,
            'fa-cut': true,
            disabled: selectedfileSystemObject ? selectedfileSystemObject.locked : true
        };
    }

    getPasteButtonClass() {
        return {
            'modal-trigger': this.cutFileSystemObject ? true : false,
            fas: true,
            'fa-paste': true,
            disabled: this.cutFileSystemObject && this.cutBreadcrumbs.length !== this.breadcrumbs.length ? false : true
        };
    }

    getDeleteButtonClass() {
        const selectedfileSystemObject = this.fileSystemObjects ? this.fileSystemObjects.find(x => x.isSelected) : null;
        return {
            'modal-trigger': selectedfileSystemObject ? !(selectedfileSystemObject.locked || selectedfileSystemObject.isCut) : false,
            fas: true,
            'fa-trash-alt': true,
            disabled: selectedfileSystemObject ? (selectedfileSystemObject.locked || selectedfileSystemObject.isCut) : true
        };
    }

    getRenameButtonClass() {
        const selectedfileSystemObject = this.fileSystemObjects ? this.fileSystemObjects.find(x => x.isSelected) : null;
        return {
            'modal-trigger': selectedfileSystemObject ? !(selectedfileSystemObject.locked || selectedfileSystemObject.isCut) : false,
            fas: true,
            'fa-i-cursor': true,
            disabled: selectedfileSystemObject ? (selectedfileSystemObject.locked || selectedfileSystemObject.isCut) : true
        };
    }



    getDownloadButtonClass() {
        const selectedfileSystemObject = this.fileSystemObjects ? this.fileSystemObjects.find(x => x.isSelected) : null;
        return {
            fas: true,
            'fa-cloud-download-alt': true,
            disabled: selectedfileSystemObject ? selectedfileSystemObject.type.toLocaleLowerCase() !== 'file' : true
        };
    }

    getShareButtonClass() {
        return {
            'modal-trigger': this.breadcrumbs ? !(this.breadcrumbs.length <= 1 && this.fileSystemObjects.filter(x => x.isSelected).length === 0) : false,
            fas: true,
            'fa-share': true,
            disabled: this.breadcrumbs ? (this.breadcrumbs.length <= 1 && this.fileSystemObjects.filter(x => x.isSelected).length === 0) : true
        };
    }

    getProgressBarStyle(storageSpace: StorageSpace) {
        const percent = (storageSpace.used / storageSpace.limit) * 100;
        let color = 'rgba(0, 255, 0, 0.4)';
        if (percent > 70) {
            color = 'rgba(255, 0, 0, 0.4)';
        }

        return {
            'width.%': percent,
            'background-color': color
        };
    }



    fileSystemObjectOnClick(event: any, fileSystemObject: FileSystemObject) {
        if (!this.isMobileBrowser) {
            this.ToogleFileSystemObjectSelectStatus(fileSystemObject);
        } else {
            if (fileSystemObject.type !== 'file') {
                this.changeDirectory.emit(fileSystemObject.relativepath);
            } else {
                this.ViewDocument(fileSystemObject);
            }
        }
    }

    fileSystemObjectOnDblClick(event: any, fileSystemObject: FileSystemObject) {
        if (!this.isMobileBrowser) {
            if (fileSystemObject.type !== 'file') {
                this.changeDirectory.emit(fileSystemObject.relativepath);
            } else {
                this.ViewDocument(fileSystemObject);
            }
        } else {
            console.log('mobile device dblclick actions'); // currently nothing planned
        }
    }

    fileSystemObjectOnTouchStart(event: any, fileSystemObject: FileSystemObject) {
        this.isTouching = true;
        setTimeout(() => {
            if (this.isTouching) {
                this.timeoutHandler = 0;
            }
        }, this.timeoutHandler);
    }

    fileSystemObjectOnTouchEnd(event: any, fileSystemObject: FileSystemObject) {
        this.isTouching = false;
        if (this.timeoutHandler === 0) {
            this.ToogleFileSystemObjectSelectStatus(fileSystemObject);
        }
        this.timeoutHandler = 600;
    }

    breadcrumbOnClick(event: any, breadcrumb: Breadcrumb) {
        this.changeDirectory.emit(breadcrumb.url);
    }

    levelUpButtonOnClick(event: any) {
        this.changeDirectory.emit(this.breadcrumbs[this.breadcrumbs.length - 2].url);
    }

    newDirectoryButtonOnClick(event: any) {
        this.newDirectoryForm.controls['name'].setValue('');
    }

    cutButtonOnClick(event: any) {
        const fileSystemObject = this.fileSystemObjects.find(x => x.isSelected);
        if (fileSystemObject.isCut) {
            fileSystemObject.isCut = false;
            this.cutFileSystemObject = null;
            this.cutBreadcrumbs = null;
        } else {
            this.fileSystemObjects.filter(x => x.isCut === true).map(x => x.isCut = false);
            fileSystemObject.isCut = true;
            this.cutFileSystemObject = fileSystemObject;
            this.cutBreadcrumbs = this.breadcrumbs;
        }
    }

    renameButtonOnClick(event: any) {
        const fileSystemObject = this.fileSystemObjects.find(x => x.isSelected);
        console.log(fileSystemObject.name);
        this.renameForm.controls['name'].setValue(fileSystemObject.name);
    }

    downloadButtonOnClick(event: any) {
        const fileSystemObject = this.fileSystemObjects.find(x => x.isSelected);
        this.downloadFile.emit({
            fileId: fileSystemObject.id
        });
    }

    shareButtonOnClick(event: any) {
        const fileSystemObject = this.fileSystemObjects.find(x => x.isSelected);
        console.log(fileSystemObject.name);
    }

    inputFieldOnKeyup(event: any) {
        const searchFor = event.target.value.toLocaleLowerCase();
        this.fileSystemObjects.map(x => x.isVisiable = true);
        if (searchFor && searchFor !== '') {
            this.fileSystemObjects.filter(x => !x.displayName.toLocaleLowerCase().match(searchFor)).map(x => x.isVisiable = false);
        }
    }

    uploadDocumentInputChangeEvent(event: any) {
        const fileToUpload = <File[]>event.target.files[0];
        this.uploadFileInformation.name = (<any>fileToUpload).name;
        this.uploadFileInformation.type = (<any>fileToUpload).type;
        this.uploadFileInformation.lastModifiedDate = (<any>fileToUpload).lastModifiedDate;
        this.documentBlob = new Blob([fileToUpload], { type: this.uploadFileInformation.type });
    }

    uploadDocumentButtonOnClick(event: any) {
        this.uploadDocument.emit({
            documentBlob: this.documentBlob,
            documentInfo: this.uploadFileInformation,
            path: this.breadcrumbs[this.breadcrumbs.length - 1].url + '/'
        });
    }

    newDirectoryFormOnSubmit(event: Event, formData: any) {
        this.newDirectory.emit({
            name: formData.name,
            path: this.breadcrumbs[this.breadcrumbs.length - 1].url
        });
    }

    pasteModalOnYesClick(event: any) {
        let object: any = {
            name: '',
            type: this.cutFileSystemObject.type,
            sourceDir: this.cutBreadcrumbs[this.cutBreadcrumbs.length - 1].url,
            currentDir: this.breadcrumbs[this.breadcrumbs.length - 1].url
        }
        if (this.cutFileSystemObject.type === 'file') {
            object.name = this.cutFileSystemObject.name + '.' + this.cutFileSystemObject.extension;
        } else {
            object.name = this.cutFileSystemObject.name;
        }
        this.pasteFileSystemObject.emit(object);
        this.cutBreadcrumbs = null;
        this.cutFileSystemObject = null;
    }

    deleteModalOnYesClick(event: any) {
        const fileSystemObject = this.fileSystemObjects.find(x => x.isSelected);
        this.deleteFileSystemObject.emit({
            name: fileSystemObject.name,
            path: this.breadcrumbs[this.breadcrumbs.length - 1].url,
            type: fileSystemObject.type
        });
    }

    renameFormOnSubmit(event: Event, formData: any) {
        const fileSystemObject = this.fileSystemObjects.find(x => x.isSelected);
        const fileSystemObjectWithSameName = this.fileSystemObjects.find(x => x.type === fileSystemObject.type && x.name.toLocaleLowerCase() === formData.name.toLocaleLowerCase());
        if (fileSystemObjectWithSameName) {
            console.log('file system object with same name found.');
        } else {
            this.renameFileSystemObject.emit({
                name: fileSystemObject.name,
                newName: formData.name,
                extension: fileSystemObject.extension,
                type: fileSystemObject.type,
                path: this.breadcrumbs[this.breadcrumbs.length - 1].url
            });
        }
    }
}
