export class FileSystemObject {
    public id: string;
    public versionNumber: number;
    public name: string;
    public type: string;
    public extension: string;
    public displayName: string;
    public path: string;
    public relativepath: string;
    public fullPath: string;
    public fileUrl: string;
    public userId: string;
    public locked: boolean;

    public isShared: boolean;
    public isSelected: boolean;
    public isCut: boolean;
    public isVisiable: boolean;

    public constructor(init?: Partial<FileSystemObject>) {
        Object.assign(this, init);
    }
}

export class StorageSpace {
    public limit: number;
    public limitUnit: string;
    public displayLimit: number;
    public displayLimitUnit: string;
    public used: number;
    public usedUnit: string;
    public displayUsed: number;
    public displayUsedUnit: string;

    public constructor(init?: Partial<StorageSpace>) {
        Object.assign(this, init);
    }
}

export class Breadcrumb {
    public name: string;
    public url: string;
    public relativeUrl: string;

    public constructor(init?: Partial<Breadcrumb>) {
        Object.assign(this, init);
    }
}

export class UploadFileInformation {
    name: string;
    type: string;
    lastModifiedDate: Date;

    public constructor(init?: Partial<UploadFileInformation>) {
        Object.assign(this, init);
    }
}
export class ShareOption {
    id: string;
    name: string;
    description: string;
    hasDropDown: boolean;
    dropDownValue: string;
    dropDownOption: ShareDropdownOption[];

    public constructor(init?: Partial<ShareOption>) {
        Object.assign(this, init);
    }
}

export class ShareDropdownOption {
    id: string;
    value: string;

    public constructor(init?: Partial<ShareDropdownOption>) {
        Object.assign(this, init);
    }
}